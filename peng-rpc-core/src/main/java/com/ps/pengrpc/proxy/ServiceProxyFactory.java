package com.ps.pengrpc.proxy;

import com.ps.pengrpc.RpcApplication;

import java.lang.reflect.Proxy;

public class ServiceProxyFactory {
    /**
     * 根据服务类获取代理对象
     *
     * @param serviceClass
     * @return
     * @param <T>
     */
    public static <T> T getProxy(Class<T> serviceClass){

        if(RpcApplication.getRpcConfig().getMock()){
            return getMockProxy(serviceClass);
        }else{
            return (T) Proxy.newProxyInstance(serviceClass.getClassLoader(),
                    new Class[]{serviceClass},
                    new ServiceProxy());
        }
    }

    /**
     * 根据服务类获取 Mock 代理对象
     *
     * @param serviceClass
     * @return
     * @param <T>
     */
    public static <T> T getMockProxy(Class<T> serviceClass){
        return (T) Proxy.newProxyInstance(
          serviceClass.getClassLoader(),
          new Class[]{serviceClass},
          new MockServiceProxy());

    }
}
