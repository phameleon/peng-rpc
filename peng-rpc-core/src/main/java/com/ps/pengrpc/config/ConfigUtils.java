package com.ps.pengrpc.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.setting.dialect.Props;
import com.ps.pengrpc.constant.ApplicationSuffixConstant;

/**
 * 配置工具类
 */
public class ConfigUtils {
    public static <T> T loadConfig(Class<T> tClass,String prefix){
        return loadConfig(tClass,prefix,"","");
    }


    /**
     * 支持不同类型的配置文件
     *
     * @param tClass
     * @param prefix
     * @param suffix
     * @return
     * @param <T>
     */
    public static <T> T loadConfig(Class<T> tClass, String prefix,String suffix){
        return loadConfig(tClass,prefix,suffix,"");
    }


    /**
     * 加载配置对象，支持区分环境
     *
     * @param tClass
     * @param prefix
     * @param suffix
     * @param environment
     * @return
     * @param <T>
     */
    public static <T> T loadConfig(Class<T> tClass,String prefix,String suffix,String environment){
        StringBuilder configFileBuilder = new StringBuilder("application");
        if(StrUtil.isNotBlank(environment)){
            configFileBuilder.append("-").append(environment);
        }
        if(StrUtil.isNotBlank(suffix)){
            configFileBuilder.append(".").append(suffix);
        }else {
            configFileBuilder.append(ApplicationSuffixConstant.DEFAULT_APPLICATION_SUFFIX);
        }

        Props props = new Props(configFileBuilder.toString());
        return props.toBean(tClass,prefix);
    }


}
