package com.ps.pengrpc.config;

import lombok.Data;

/**
 * PRC 框架注册中心配置
 */
@Data
public class RegistryConfig {

    /**
     * 注册中心类别
     */
//    private String register = "etcd";
    private String register = "zookeeper";

    /**
     * 注册中心地址
     */
//    private String address = "http://192.168.66.134:2380"; //etcd
    private String address = "192.168.66.134:2181"; //zookeeper

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 超时时间（单位毫秒）
     */
    private Long timeout = 10000L;
}
