package com.ps.pengrpc.fault.retry;

import com.github.rholder.retry.*;
import com.ps.pengrpc.model.RpcResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * 固定时间间隔 - 重试策略
 */
@Slf4j
public class FixedIntervalRetryStrategy implements RetryStrategy {
    /**
     * 重试
     *
     * @param callable
     * @return
     * @throws Exception
     */
    @Override
    public RpcResponse doRetry(Callable<RpcResponse> callable) throws Exception {
        Retryer<RpcResponse> retryer = RetryerBuilder
                .<RpcResponse>newBuilder()
                .retryIfExceptionOfType(Exception.class) //指定当抛出的异常是 'Exception' 类型或其子类时，应该进行重试。
                .withWaitStrategy(WaitStrategies.fixedWait(3L, TimeUnit.SECONDS)) //设置等待策略。每次重试之前会等待 3 秒。
                .withStopStrategy(StopStrategies.stopAfterAttempt(3)) //设置停止策略。最多重试 3 次。
                .withRetryListener(new RetryListener() {
                    @Override
                    public <V> void onRetry(Attempt<V> attempt) {
                        log.info("重试次数{}", attempt.getAttemptNumber());
                    }
                }).build();

        return retryer.call(callable);
    }
}
