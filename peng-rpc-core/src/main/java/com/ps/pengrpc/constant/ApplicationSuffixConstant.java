package com.ps.pengrpc.constant;

/**
 * 配置文件后缀   常量
 */
public interface ApplicationSuffixConstant {
    /**
     * 默认配置文件加载前缀
     */
    String DEFAULT_APPLICATION_SUFFIX = ".properties";

    String YAML_APPLICATION_SUFFIX = ".yaml";
    String YML_APPLICATION_SUFFIX = ".yml";
    String XML_APPLICATION_SUFFIX = ".xml";

}
