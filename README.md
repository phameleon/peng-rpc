# peng-rpc

#### 介绍
手写RPC

基于 Java + Etcd + Vert.x + 自定义协议实现。开发者可以引入Spring Boot Starter，通过注解和配置文件快速使用框架，像调用本地方法一样轻松调用远程服务；还支持通过SPI机制动态扩展序列化器、负载均衡器、重试和容错策略。


