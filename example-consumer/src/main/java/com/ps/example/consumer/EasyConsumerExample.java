package com.ps.example.consumer;

import com.ps.example.common.model.User;
import com.ps.example.common.service.UserService;
import com.ps.pengrpc.proxy.ServiceProxy;
import com.ps.pengrpc.proxy.ServiceProxyFactory;

public class EasyConsumerExample {
    public static void main(String[] args) {
        //todo 需要获取 UserService 的实现类对象
        //静态代理
//        UserService userService = new UserServiceProxy();
        //动态代理
        UserService userService = ServiceProxyFactory.getProxy(UserService.class);
        User user = new User();
        user.setName("ps");
        //调用
        User newUser = userService.getUser(user);
        if(newUser != null){
            System.out.println(newUser.getName());
        }else{
            System.out.println("user == null");
        }
    }
}
