package com.ps.example.consumer;

import com.ps.example.common.model.User;
import com.ps.example.common.service.UserService;
import com.ps.pengrpc.proxy.ServiceProxyFactory;


/**
 * 简易服务消费者示例
 */
public class ConsumerExample {
    public static void main(String[] args) {
        //获取代理
        UserService userService = ServiceProxyFactory.getProxy(UserService.class);
        User user = new User();
        user.setName("peng");

        //调用
        User newUser = userService.getUser(user);
        if(newUser != null){
            System.out.println(newUser.getName());
        }else{
            System.out.println("user == null");
        }

    }
}
