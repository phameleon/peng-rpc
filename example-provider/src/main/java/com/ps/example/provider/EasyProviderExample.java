package com.ps.example.provider;

import com.ps.example.common.service.UserService;
import com.ps.pengrpc.registry.LocalRegistry;
import com.ps.pengrpc.server.HttpServer;
import com.ps.pengrpc.server.VertxHttpServer;

/**
 * 简易服务提供者
 */
public class EasyProviderExample {
    public static void main(String[] args) {
        //注册服务
        LocalRegistry.register(UserService.class.getName(), UserServiceImpl.class);

        //启动 web 服务
        HttpServer httpServer = new VertxHttpServer();
        httpServer.doStart(8080);
    }
}
