# Docker 安装 etcd（集群）、etcd-keeper

## 安装etcd

### 创建一个新的bridge网络

docker network create --driver bridge --subnet=10.2.36.0/16 --gateway=10.2.36.1 peng-rpc



### 拉取etcd镜像

docker pull quay.io/coreos/etcd



### 运行容器

节点1

docker run -d \
-p 2379:2379 \
-p 2380:2380 \
--name node1 \
--network=peng-rpc \
--ip 10.2.36.2 \
quay.io/coreos/etcd:latest \
etcd \
-name node1 \
-advertise-client-urls http://10.2.36.2:2379 \
-initial-advertise-peer-urls http://10.2.36.2:2380 \
-listen-client-urls http://0.0.0.0:2379 -listen-peer-urls http://0.0.0.0:2380 \
-initial-cluster-token etcd-cluster \
-initial-cluster "node1=http://10.2.36.2:2380,node2=http://10.2.36.3:2380,node3=http://10.2.36.4:2380" \
-initial-cluster-state new

节点2

docker run -d \
-p 2479:2379 \
-p 2381:2380 \
--name node2 \
--network=peng-rpc \
--ip 10.2.36.3 \
quay.io/coreos/etcd:latest \
etcd \
-name node2 \
-advertise-client-urls http://10.2.36.3:2379 \
-initial-advertise-peer-urls http://10.2.36.3:2380 \
-listen-client-urls http://0.0.0.0:2379 -listen-peer-urls http://0.0.0.0:2380 \
-initial-cluster-token etcd-cluster \
-initial-cluster "node1=http://10.2.36.2:2380,node2=http://10.2.36.3:2380,node3=http://10.2.36.4:2380" \
-initial-cluster-state new

节点3

docker run -d \
-p 2579:2379 \
-p 2382:2380 \
--name node3 \
--network=peng-rpc \
--ip 10.2.36.4 \
quay.io/coreos/etcd:latest \
etcd \
-name node3 \
-advertise-client-urls http://10.2.36.4:2379 \
-initial-advertise-peer-urls http://10.2.36.4:2380 \
-listen-client-urls http://0.0.0.0:2379 -listen-peer-urls http://0.0.0.0:2380 \
-initial-cluster-token etcd-cluster \
-initial-cluster "node1=http://10.2.36.2:2380,node2=http://10.2.36.3:2380,node3=http://10.2.36.4:2380" \
-initial-cluster-state new

### 通过docker ps命令查看成功运行的容器

CONTAINER ID   IMAGE                        COMMAND                   CREATED          STATUS          PORTS                                                                                  NAMES
39e61454c9da   evildecay/etcdkeeper         "/bin/sh -c './etcdk…"   10 minutes ago   Up 10 minutes   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                                              etcd-keeper
596bb1ff4e93   quay.io/coreos/etcd:latest   "etcd -name node3 -a…"   43 minutes ago   Up 43 minutes   0.0.0.0:2579->2379/tcp, :::2579->2379/tcp, 0.0.0.0:2382->2380/tcp, :::2382->2380/tcp   node3
af0d8c376195   quay.io/coreos/etcd:latest   "etcd -name node2 -a…"   45 minutes ago   Up 45 minutes   0.0.0.0:2479->2379/tcp, :::2479->2379/tcp, 0.0.0.0:2381->2380/tcp, :::2381->2380/tcp   node2
27c79ec0b788   quay.io/coreos/etcd:latest   "etcd -name node1 -a…"   50 minutes ago   Up 50 minutes   0.0.0.0:2379-2380->2379-2380/tcp, :::2379-2380->2379-2380/tcp                          node1

### 通过etcdctl member list命令可以查询出所有集群节点的列表即为成功
docker exec -it 27c etcdctl member list
204d370525df28af: name=node3 peerURLs=http://10.2.36.4:2380 clientURLs=http://10.2.36.4:2379 isLeader=false
2db3381ff302973d: name=node2 peerURLs=http://10.2.36.3:2380 clientURLs=http://10.2.36.3:2379 isLeader=false
ffbd0562b8dd25e9: name=node1 peerURLs=http://10.2.36.2:2380 clientURLs=http://10.2.36.2:2379 isLeader=true

#### 无论通过哪一集群节点查询出来的结果都应相同
$docker exec -it af0 etcdctl member list
204d370525df28af: name=node3 peerURLs=http://10.2.36.4:2380 clientURLs=http://10.2.36.4:2379 isLeader=false
2db3381ff302973d: name=node2 peerURLs=http://10.2.36.3:2380 clientURLs=http://10.2.36.3:2379 isLeader=false
ffbd0562b8dd25e9: name=node1 peerURLs=http://10.2.36.2:2380 clientURLs=http://10.2.36.2:2379 isLeader=true

$docker exec -it 596 etcdctl member list
204d370525df28af: name=node3 peerURLs=http://10.2.36.4:2380 clientURLs=http://10.2.36.4:2379 isLeader=false
2db3381ff302973d: name=node2 peerURLs=http://10.2.36.3:2380 clientURLs=http://10.2.36.3:2379 isLeader=false
ffbd0562b8dd25e9: name=node1 peerURLs=http://10.2.36.2:2380 clientURLs=http://10.2.36.2:2379 isLeader=true



## 安装etcd-keeper

### 拉取etcd-keeper镜像

docker pull evildecay/etcdkeeper



### 检查etcd容器ip

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' node1



### 运行容器


docker run -d -p 8080:8080 -e ETCD_SERVERS=http://10.2.36.2:2379 --network=peng-rpc --name etcd-keeper evildecay/etcdkeeper



### 浏览器验证是否成功启动

![image-20240506103107778](C:\Users\peng\AppData\Roaming\Typora\typora-user-images\image-20240506103107778.png)