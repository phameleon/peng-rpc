# Linux 安装 zookeeper

## 下载压缩包

wget https://dlcdn.apache.org/zookeeper/zookeeper-3.8.4/apache-zookeeper-3.8.4-bin.tar.gz

## 解压

tar -zxvf apache-zookeeper-3.8.4-bin.tar.gz

## 配置conf文件

cp zoo_sample.cfg zoo.cfg

vim zoo.cfg



### zoo.cfg配置信息（默认也可以）

zookeeper内部的基本单位，单位是毫秒，这个表示一个tickTime为2000毫秒，在zookeeper的其他配置中，都是基于tickTime来做换算的

tickTime=2000



#集群中的follower服务器(F)与leader服务器(L)之间 初始连接 时能容忍的最多心跳数（tickTime的数量）。
initLimit=10



#syncLimit：集群中的follower服务器(F)与leader服务器(L)之间 请求和应答 之间能容忍的最多心跳数（tickTime的数量）
syncLimit=5



数据存放文件夹，zookeeper运行过程中有两个数据需要存储，一个是快照数据（持久化数据）另一个是事务日志

dataDir=/tmp/zookeeper



客户端访问端口

clientPort=2181

### 修改zoo_sample.cfg

vim zoo_sample.cfg

![image-20240512093843661](C:\Users\peng\AppData\Roaming\Typora\typora-user-images\image-20240512093843661.png)

## 配置环境变量

cd /etc

vim profile

添加环境变量
#zookeeper
export ZOOKEEPER_HOME=/root/apache-zookeeper-3.8.4-bin
export PATH=$PATH:${ZOOKEEPER_HOME}/bin

编辑完，使用命令使配置生效

source /etc/profile